#! /usr/bin/env python3


"""Sort emails by attachment size."""

from datetime import datetime
import os
import sys
from operator import itemgetter

from notmuch import Database, Query


def formatted_message_id(message):
    return "id:{}".format(message.get_message_id())


def get_file_size(message):
    file_path = message.get_filename()
    file_size = os.path.getsize(file_path)
    return file_size


def get_summary(message, file_size):
    date_str = datetime.fromtimestamp(message.get_date())
    return " - ".join(map(str, [
        formatted_message_id(message),
        human_file_size(file_size),
        date_str,
        message.get_header("subject"),
    ]))


def human_file_size(nbytes):
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    i = 0
    while nbytes >= 1024 and i < len(suffixes) - 1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, suffixes[i])


def message_and_file_size(message):
    return (message, get_file_size(message))


def main():
    db = Database()
    messages = db.create_query("tag:attachment AND NOT tag:deleted").search_messages()
    message_and_file_size_sequence = sorted(
        map(message_and_file_size, messages),
        key=itemgetter(1),
        reverse=True,
    )
    print("\n".join(map(lambda x: get_summary(x[0], x[1]), message_and_file_size_sequence)))
    return 0


if __name__ == "__main__":
    try:
        status = main()
    except BrokenPipeError:
        pass
    else:
        sys.exit(status)
