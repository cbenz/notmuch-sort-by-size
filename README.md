# Notmuch sort by size

This program is made for [Notmuch](https://notmuchmail.org/).

Goal: sort emails with attachments by size.
This can be useful to find out which ones to delete first to free some space.

## Install

If [Notmuch](https://notmuchmail.org/) is installed on your machine, it should be OK, because it is shipped with the `notmuch` Python module.

To check:

```sh
pip install -r requirements.txt

# Should display:
Requirement already satisfied: notmuch>=0.25 in /usr/lib/python3.6/site-packages (from -r requirements.txt (line 1))
```

## Usage

```sh
./notmuch-sort-by-size.py
```

Output:

```
id:xxxxxxxxxxxxxxxxxx+yyyyyyyyyyyyyyyyyyy-wwwwwwwwwww-zzzzzzzzzz@mail.gmail.com - 14.42 MB - 2010-01-01 20:50:06 - Photos of my dog
id:234565467890987876543234565432@FHJTRDSXKJVGTF.eurprd01.prod.outlook.com - 10.51 MB - 2013-08-01 11:33:31 - Photos of your dog
```

To view emails, if you use a client compatible with Notmuch, like [Astroid](http://astroidmail.github.io/), you can search for `id:xxxxxxxxxx`.

To delete emails from command line, see the ["excluding" page on Notmuch documentation](https://notmuchmail.org/excluding/). In short:

```sh
notmuch search --format=text0 --output=files id:xxxxxxxxxx | xargs -0 --no-run-if-empty rm
```
